import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TicTacToe } from './tic-tac-toe';
import { BoardModule } from "../../components/board/board.module";

@NgModule({
  declarations: [
    TicTacToe,
  ],
  imports: [
    IonicPageModule.forChild(TicTacToe),
    BoardModule
  ],
  exports: [
    TicTacToe
  ]
})
export class TicTacToeModule {}
