import { Component } from '@angular/core';

/**
 * Generated class for the Board component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'board',
  templateUrl: 'board.html'
})
export class Board {

  text: string;

  constructor() {
    console.log('Hello Board Component');
    this.text = 'IMMA BOARD!';
  }

}
