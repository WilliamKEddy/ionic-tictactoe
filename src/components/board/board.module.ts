import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Board } from './board';

@NgModule({
  declarations: [
    Board,
  ],
  imports: [
    IonicPageModule.forChild(Board),
  ],
  exports: [
    Board
  ]
})
export class BoardModule {}
